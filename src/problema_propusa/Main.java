package problema_propusa;
import java.util.Scanner;



public class Main {

	public static int verificare(int nr)
	{
		int d=2;
		while(d<=nr/2)
		{
			if(nr%d==0)
				return 0;
			d++;
		}
		return 1;
	}
	
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int limitaInf = in.nextInt();
		int limitaSup = in.nextInt();

		System.out.println("Numerele prime din interval sunt: ");
		for(int i=limitaInf;i<=limitaSup;i++)
		{
			int ok=verificare(i);
			if(ok==1)
			System.out.println(" "+i);
		}
		
		
	}

}
